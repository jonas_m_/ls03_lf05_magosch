import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JColorChooser;
import javax.swing.JTextField;

public class form_frame extends JFrame {

	private JPanel contentPane;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					form_frame frame = new form_frame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public form_frame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 576);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 432, 529);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Dieser Text soll ver\u00E4ndert werden");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(12, 40, 408, 16);
		panel.add(lblNewLabel);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.setBackground(Color.RED);
			}
		});
		btnRot.setBounds(12, 108, 122, 25);
		panel.add(btnRot);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.GREEN);
			}
		});
		btnGrn.setBounds(158, 108, 119, 25);
		panel.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.BLUE);
			}
		});
		btnBlau.setBounds(301, 108, 119, 25);
		panel.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(Color.YELLOW);
			}
		});
		btnGelb.setBounds(12, 136, 122, 25);
		panel.add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setBackground(new Color(0xEEEEEE));
			}
		});
		btnStandardfarbe.setBounds(158, 136, 119, 25);
		panel.add(btnStandardfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color ausgewaehlteFarbe = JColorChooser.showDialog(null, "Farbauswahl", null);
				panel.setBackground(ausgewaehlteFarbe);
			}
		});
		btnFarbeWhlen.setBounds(301, 136, 119, 25);
		panel.add(btnFarbeWhlen);
		
		JLabel lblAufgabeText = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabeText.setBounds(12, 174, 187, 16);
		panel.add(lblAufgabeText);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.setFont(new Font("Arial", Font.PLAIN, 12));
			}
		});
		btnArial.setBounds(12, 194, 122, 25);
		panel.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		btnComicSansMs.setBounds(158, 194, 119, 25);
		panel.add(btnComicSansMs);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		btnCourierNew.setBounds(301, 194, 119, 25);
		panel.add(btnCourierNew);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(12, 232, 408, 22);
		panel.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblNewLabel.setText(txtHierBitteText.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(12, 256, 209, 25);
		panel.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setText("");
			}
		});
		btnTextImLabel.setBounds(222, 256, 198, 25);
		panel.add(btnTextImLabel);
		
		JLabel lblAufgabeSchriftfarbe = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabeSchriftfarbe.setBounds(12, 294, 187, 16);
		panel.add(lblAufgabeSchriftfarbe);
		
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.RED);
			}
		});
		btnRot_1.setBounds(12, 323, 122, 25);
		panel.add(btnRot_1);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLUE);
			}
		});
		btnBlau_1.setBounds(158, 323, 122, 25);
		panel.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setForeground(Color.BLACK);
			}
		});
		btnSchwarz.setBounds(301, 323, 122, 25);
		panel.add(btnSchwarz);
		
		JLabel lblAufgabeSchriftgre = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabeSchriftgre.setBounds(12, 361, 224, 16);
		panel.add(lblAufgabeSchriftgre);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinksbndig.setBounds(12, 431, 122, 25);
		panel.add(btnLinksbndig);
		
		JLabel lblAufgabeSchriftgre_1 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeSchriftgre_1.setBounds(12, 413, 224, 16);
		panel.add(lblAufgabeSchriftgre_1);
		
		JButton button_4 = new JButton("+");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int groesse = lblNewLabel.getFont().getSize();
//				lblNewLabel.setFont(new Font(�Arial�, Font.PLAIN, groesse + 1));
			}
		});
		button_4.setBounds(12, 382, 209, 25);
		panel.add(button_4);
		
		JButton button_5 = new JButton("-");
		button_5.setBounds(222, 382, 198, 25);
		panel.add(button_5);
		
		JLabel lblAufgabeHintergrundfarbe = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabeHintergrundfarbe.setBounds(12, 89, 252, 16);
		panel.add(lblAufgabeHintergrundfarbe);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(155, 431, 122, 25);
		panel.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setBounds(298, 431, 122, 25);
		panel.add(btnRechtsbndig);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setBounds(12, 491, 408, 25);
		panel.add(btnExit);
		
		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabeProgramm.setBounds(12, 469, 187, 16);
		panel.add(lblAufgabeProgramm);
	}
}
