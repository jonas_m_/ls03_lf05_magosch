package controller;

import java.util.Vector;

import com.sun.prism.impl.Disposer.Target;

import model.Level;
import model.persistance.IAttemptPersistance;
import model.persistance.IPersistance;


public class AttemptController {

	private IAttemptPersistance attemptPersistance;

	/**
	 * erstellt ein neues Objekt eines AttemptController welches Attemptobjekte in
	 * der übergebenen Datenhaltung persisiert
	 * 
	 * @param alienDefenceModel.getAttemptDB()
	 *            Persistenzklasse der Attemptobjekte
	 */
	public AttemptController(IPersistance alienDefenceModel) {
		this.attemptPersistance = alienDefenceModel.getAttemptPersistance();
	}

	public Vector<Vector<String>> getAllAttemptsPerLevel(int level_id, int game_id) {
		return attemptPersistance.getAllAttemptsPerLevel(level_id, game_id);
	}

	public int getPlayerPosition() {
		return attemptPersistance.getPlayerPosition();
	}

	public void deleteHighscore(int level_id) {
		attemptPersistance.deleteHighscore(level_id);
	}

	/**
	 * calculates points from attempt for highscore TODO create formula here
	 * 
	 * @param level Levelobjekt
	 * @param hitcounter Controllerobjekt das die Treffer und Reaktionszeiten misst
	 * @return points 
	 */
	int highscore = 0;
	public int calculatePoints(Level level, HitCounter hitcounter) {
		highscore = ((hitcounter.getHit()/hitcounter.getTargets())*400)+((hitcounter.getHit()/hitcounter.getShots())*200)+(1000-(hitcounter.getSumReactionDiffernce())*400);
		return highscore;
	}
}
